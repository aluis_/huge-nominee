import './main.scss';

import 'whatwg-fetch';
// import 'es6-promisse';
import Vue from 'vue';
import App from './App.vue';
import 'jquery-touch-events';
import 'assets/img/favicon_nominees.ico';

new Vue({
  el: '#app',
  render: h => h(App)
})