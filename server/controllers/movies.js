var moviesBlueprint = require('../blueprints/movies.js');

var MoviesList = {
    getMoviesList: function(req, res) {
        res.send(moviesBlueprint);
    }
};

module.exports = MoviesList;