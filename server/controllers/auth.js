var passport = require('passport'),
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    User = require('../models/user.js').User,
    googleConfig,
    isProduction = process.env.NODE_ENV === "production";

googleConfig = {
    clientID: "508898436271-ofhknoa7coebqrul68bgn7fsin7pmi03.apps.googleusercontent.com",
    clientSecret: "gdmEyD-XYL5k1cCoSYKA_Ejk",
    callbackURL: isProduction ? 'http://nominees.hugeinc.com/oauth2callback' : 'http://localhost:3000/oauth2callback'
};

module.exports = function(app) {
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use(new GoogleStrategy(googleConfig, function(token, refreshToken, profile, done) {
        process.nextTick(function() {
            User.findOne({ 'googleId' : profile.id }, function(err, user) {
                if (err)
                    return done(err);

                if (user) {
                    user.token = token;
                    user.image = profile.photos && profile.photos[0] ? profile.photos[0].value : '';

                    user.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, user);
                    });

                } else {
                    var newUser = new User();

                    newUser.googleId = profile.id;
                    newUser.token = token;
                    newUser.name  = profile.displayName;
                    newUser.email = profile.emails[0].value;
                    newUser.image = profile.photos && profile.photos[0] ? profile.photos[0].value : '';
                    newUser.votes = {};

                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
                
            });
        });

    }));

    // TODO: separate routes
    app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

    app.get('/oauth2callback', passport.authenticate('google', {
            successRedirect : '/',
            failureRedirect : '/404.html'
        }
    ));

    app.get('/logout', function(req, res) {
        req.logout();
        res.send({logout: true})
    });

    app.get('/api', isLoggedIn, function(req, res) {
        res.json({
            name: req.user.name,
            email: req.user.email,
            image: req.user.image,
            token : req.user.token,
            votes: req.user.votes
        });
    });

    app.post('/api/vote', function(req, res) {
        var category = req.body.category,
            movie = req.body.movie;

        User.findOne({ 'token' : req.body.token }, function (err, user) {
            if (!user) {
                req.logout();
                res.send({});
                return;
            }

            if (err) {
                return handleError(err)
            }

            var modifiedVotes = Object.assign({}, user.votes);
            modifiedVotes[category] = movie;

            user.votes = Object.assign({}, modifiedVotes);
            user.save(function (err, updatedUser) {
                if (err) return handleError(err);
                res.send(updatedUser.votes);
            });
        });
    });

    app.get('/api/movies-list', function(req, res) {
        var moviesBlueprint = require('../blueprints/movies.js');

        User.find({}, function (err, users) {
            var allVotes;

            if (err) {
                return handleError(err)
            }

            allVotes = {};

            users.forEach((user) => {
                if (user.votes || user.votes >= 0) {
                    for (vote in user.votes) {
                        allVotes[vote] = allVotes[vote] ? allVotes[vote] : {
                            total: 0
                        };

                        allVotes[vote].total++;

                        allVotes[vote][user.votes[vote]] = allVotes[vote][user.votes[vote]] ? allVotes[vote][user.votes[vote]] + 1 : 1;
                    }
                }
            });

            res.send({
                categories: moviesBlueprint,
                allVotes: allVotes
            });
        });
    });

    function isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) {
                return next();
        }
    }
};
