var path = require("path");
var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session')
var cookieParser = require('cookie-parser')

var passport = require('passport');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

var isProduction = process.env.NODE_ENV === "production";

app.use(function(req, res, next) {
  // res.header("Access-Control-Allow-Origin", "*");
  // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(cookieParser());
app.set('trust proxy', 1);
app.use(session({secret: 'keyboard cat', cookie: { maxAge: 86400000 }})); // TODO: control client sessions
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

var AuthController = require('./controllers/auth.js');
var NomineeController = require('./controllers/movies.js');

if (isProduction) {
    app.use(express.static(path.resolve('dist')));
} else {
    var webpack = require('webpack');
    var webpackConfig = require('../webpack.config');
    var compiler = webpack(webpackConfig);

    app.use(require("webpack-dev-middleware")(compiler, {
        noInfo: true, publicPath: webpackConfig.output.publicPath
    }));

    app.use(require("webpack-hot-middleware")(compiler));
}

AuthController(app);

app.get('/api/movies-list--old', NomineeController.getMoviesList);

app.listen(3000, function () {
  console.log("Listening on port 3000!");
});