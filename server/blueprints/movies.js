module.exports = [
     {
        _id: 'best-picture',
        name: 'Best picture.',
        movies: [
            {
                "_id": "arrival",
                "name": "Arrival",
                "poster": "arrival.jpg"
            },
            {
                "_id": "fences",
                "name": "Fences",
                "poster": "fences.jpg"
            },
            {
                "_id": "hacksawridge",
                "name": "Hacksaw Ridge",
                "poster": "hacksawridge.jpg"
            },
            {
                "_id": "hellorhighwater",
                "name": "Hell or High Water",
                "poster": "hellorhighwater.jpg"
            },
            {
                "_id": "hiddenfigures",
                "name": "Hidden Figures",
                "poster": "hiddenfigures3.jpg"
            },
            {
                "_id": "lalaland",
                "name": "La La Land",
                "poster": "lalaland.jpg"
            },
            {
                "_id": "lion",
                "name": "Lion",
                "poster": "lion3.jpg"
            },
            {
                "_id": "manchester-by-the-sea",
                "name": "Manchester by the Sea",
                "poster": "manchester4.jpg"
            },
            {
                "_id": "moonlight",
                "name": "Moonlight",
                "poster": "moonlight.jpg",
                "academyWinner": true
            }
        ]
    },
    {
        _id: 'actor-in-a-leading-role',
        name: 'Best Actor.',
        movies: [
            {
                "_id": "casey-affleck",
                "name": "Casey Affleck (Manchester by the Sea)",
                "poster": "manchester.jpg",
                "academyWinner": true
            },
            {
                "_id": "andrew-garfield",
                "name": "Andrew Garfield (Hacksaw Ridge)",
                "poster": "hacksaw2.jpg"
            },
            {
                "_id": "ryan-gosling",
                "name": "Ryan Gosling (La La Land)",
                "poster": "lalalandactor.jpg"
            },
            {
                "_id": "viggo-mortensen",
                "name": "Viggo Mortensen (Captain Fantastic)",
                "poster": "captainfantastic.jpg"
            },
            {
                "_id": "denzel-washington",
                "name": "Denzel Washington (Fences)",
                "poster": "fences2.jpg"
            }
        ]
    },
    {
        _id: 'actor-in-a-supporting-role',
        name: 'Best Supporting Actor.',
        movies: [
            {
                "_id": "mahershala-ali",
                "name": "Mahershala Ali (Moonlight)",
                "poster": "moonlight2.jpg",
                "academyWinner": true
            },
            {
                "_id": "jeff-bridges",
                "name": "Jeff Bridges (Hell or High Water)",
                "poster": "hellorhigh-support.jpg"
            },
            {
                "_id": "lucas-hedges",
                "name": "Lucas Hedges (Manchester by the Sea)",
                "poster": "manchester2.jpg"
            },
            {
                "_id": "dev-patel",
                "name": "Dev Patel (Lion)",
                "poster": "lion1.jpg"
            },
            {
                "_id": "michael-shannon",
                "name": "Michael Shannon (Nocturnal Animals)",
                "poster": "nocturnal3.jpg"
            }
        ]
    },
    {
        _id: 'actress-in-a-leading-role',
        name: 'Best Actress.',
        movies: [
            {
                "_id": "isabelle-huppert",
                "name": "Isabelle Huppert (Elle)",
                "poster": "elle.jpg"
            },
            {
                "_id": "ruth-negga",
                "name": "Ruth Negga (Loving)",
                "poster": "loving2.jpg"
            },
            {
                "_id": "natalie-portman",
                "name": "Natalie Portman (Jackie)",
                "poster": "jackie.jpg"
            },
            {
                "_id": "emma-stone",
                "name": "Emma Stone (La La Land)",
                "poster": "lalaland3.jpg",
                "academyWinner": true
            },
            {
                "_id": "meryl-streep",
                "name": "Meryl Streep (Florence Foster Jenkins)",
                "poster": "florence.jpg"
            }
        ]
    },
    {
        _id: 'actress-in-a-supporting-role',
        name: 'Best Supporting Actress.',
        movies: [
            {
                "_id": "viola-davis",
                "name": "Viola Davis (Fences)",
                "poster": "fences3.jpg",
                "academyWinner": true
            },
            {
                "_id": "naomie-harris",
                "name": "Naomie Harris (Moonlight)",
                "poster": "moonlight3.jpg"
            },
            {
                "_id": "nicole-kidman",
                "name": "Nicole Kidman (Lion)",
                "poster": "lion4.jpg"
            },
            {
                "_id": "octavia-spencer",
                "name": "Octavia Spencer (Hidden Figures)",
                "poster": "hiddenfigures2.jpg"
            },
            {
                "_id": "michelle-williams",
                "name": "Michelle Williams (Manchester by the Sea)",
                "poster": "manchester3.jpg"
            }
        ]
    },
    {
        _id: 'animated-feature-film',
        name: 'Best Animated Feature Film.',
        movies: [
            {
                "_id": "kubo",
                "name": "Kubo and the Two Strings",
                "poster": "kubo3.jpg"
            },
            {
                "_id": "moana",
                "name": "Moana",
                "poster": "moana.jpg"
            },
            {
                "_id": "my-life-as-a-zucchini",
                "name": "My Life as a Zucchini",
                "poster": "mylifezucchini.jpg"
            },
            {
                "_id": "the-red-turtle",
                "name": "The Red Turtle",
                "poster": "redturtle2.jpg"
            },
            {
                "_id": "zootopia",
                "name": "Zootopia",
                "poster": "zootopia.jpg",
                "academyWinner": true
            }
        ]
    },
    {
        _id: 'cinematography',
        name: 'Best Cinematography.',
        movies: [
            {
                "_id": "arrival",
                "name": "Arrival",
                "poster": "arrival.jpg"
            },
            {
                "_id": "lalaland",
                "name": "La La Land",
                "poster": "lalaland.jpg",
                "academyWinner": true
            },
            {
                "_id": "lion",
                "name": "Lion",
                "poster": "lion3.jpg"
            },
            {
                "_id": "moonlight",
                "name": "Moonlight",
                "poster": "moonlight.jpg"
            },
            {
                "_id": "silence",
                "name": "Silence",
                "poster": "silence.jpg"
            }
        ]
    },
    {
        _id: 'costume-design',
        name: 'Best Costume design.',
        movies: [
            {
                "_id": "allied",
                "name": "Alliedl",
                "poster": "allied.jpg"
            },
            {
                "_id": "fantastic-beasts",
                "name": "Fantastic Beasts and Where to Find Them",
                "poster": "fantasticbeasts2.jpg",
                "academyWinner": true
            },
            {
                "_id": "florence",
                "name": "Florence Foster Jenkins",
                "poster": "florence.jpg"
            },
            {
                "_id": "jackie",
                "name": "Jackie",
                "poster": "jackie.jpg"
            },
            {
                "_id": "lalaland",
                "name": "La La Land",
                "poster": "lalaland.jpg"
            }
        ]
    },
    {
        _id: 'directing',
        name: 'Best Director.',
        movies: [
            {
                "_id": "arrival",
                "name": "Denis Villeneuve (Arrival)",
                "poster": "arrival.jpg"
            },
            {
                "_id": "hacksawridge",
                "name": "Mel Gibson (Hacksaw Ridge)",
                "poster": "hacksaw.jpg"
            },
            {
                "_id": "lalaland",
                "name": "Damien Chazelle (La La Land)",
                "poster": "lalaland.jpg",
                "academyWinner": true
            },
            {
                "_id": "manchesterbythesea",
                "name": "Kenneth Lonergan (Manchester by the Sea)",
                "poster": "manchester4.jpg"
            },
            {
                "_id": "moonlight",
                "name": "Barry Jenkins (Moonlight)",
                "poster": "moonlight.jpg"
            }
        ]
    },
    {
        _id: 'documentary-feature',
        name: 'Best Documentary (feature).',
        movies: [
            {
                "_id": "13th",
                "name": "13th",
                "poster": "13th.jpg"
            },
            {
                "_id": "fire-at-sea",
                "name": "Fire at Sea",
                "poster": "fireatsea2.jpg"
            },
            {
                "_id": "i-am-not-your-negro",
                "name": "I am not Your Negro",
                "poster": "iamnotyournegro.jpg"
            },
            {
                "_id": "lifeanimated",
                "name": "Life, Animated",
                "poster": "lifeanimated.jpg"
            },
            {
                "_id": "ojamerica",
                "name": "O.J.: Made in America",
                "poster": "ojamerica.jpg",
                "academyWinner": true
            }
        ]
    },
    {
        _id: 'documentary-short',
        name: 'Best Documentary (short).',
        movies: [
            {
                "_id": "4-1-miles",
                "name": "4.1 Miles",
                "poster": "4.1miles.jpg"
            },
            {
                "_id": "extremis",
                "name": "Extremis",
                "poster": "extremis.jpg"
            },
            {
                "_id": "joes-violin",
                "name": "Joe's Violin",
                "poster": "joesviolin.jpg"
            },
            {
                "_id": "the-white-helmets",
                "name": "The White Helmets",
                "poster": "thewhitehelmets.jpg",
                "academyWinner": true
            },
            {
                "_id": "watani-my-homeland",
                "name": "Watani: My Homeland",
                "poster": "watani.jpg"
            }
        ]
    },
    {
        _id: 'film-editing',
        name: 'Best Film editing.',
        movies: [
            {
                "_id": "arrival",
                "name": "Arrival",
                "poster": "arrival.jpg"
            },
            {
                "_id": "hacksawridge",
                "name": "Hacksaw Ridge",
                "poster": "hacksaw.jpg",
                "academyWinner": true
            },
            {
                "_id": "hellorhighwater",
                "name": "Hell or High Water",
                "poster": "hellorhighwater.jpg"
            },
            {
                "_id": "lalaland",
                "name": "La La Land",
                "poster": "lalaland.jpg"
            },
            {
                "_id": "moonlight",
                "name": "Moonlight",
                "poster": "moonlight.jpg"
            }
        ]
    },
    {
        _id: 'foreign-language-film',
        name: 'Best Foreign language film.',
        movies: [
            {
                "_id": "a-man-calle-dove",
                "name": "A Man Called Dove",
                "poster": "amancalledove1.jpg"
            },
            {
                "_id": "land-of-mine",
                "name": "Land of Mine",
                "poster": "landofmine.jpg"
            },
            {
                "_id": "tanna",
                "name": "Tanna",
                "poster": "tanna2.jpg"
            },
            {
                "_id": "the-salesman",
                "name": "The Salesman",
                "poster": "thesalesman.jpg",
                "academyWinner": true
            },
            {
                "_id": "tonni-erdman",
                "name": "Tonni Erdman",
                "poster": "tonnierdman.jpg"
            }
        ]
    },
    {
        _id: 'makeup-and-hairstyling',
        name: 'Best Makeup and hairstyling.',
        movies: [
            {
                "_id": "a-man-calle-dove",
                "name": "A Man Called Dove",
                "poster": "amancalledove1.jpg"
            },
            {
                "_id": "star-trek",
                "name": "Star Trek",
                "poster": "startrek.jpg"
            },
            {
                "_id": "suicidesquad",
                "name": "Suicide Squad",
                "poster": "suicidesquad.jpg",
                "academyWinner": true
            }
        ]
    },
    {
        _id: 'music-original-score',
        name: 'Best Original score.',
        movies: [
            {
                "_id": "jackie",
                "name": "Jackie",
                "poster": "jackie.jpg"
            },
            {
                "_id": "lalaland",
                "name": "La La Land",
                "poster": "lalaland.jpg",
                "academyWinner": true
            },
            {
                "_id": "lion",
                "name": "Lion",
                "poster": "lion3.jpg"
            },
            {
                "_id": "moonlight",
                "name": "Moonlight",
                "poster": "moonlight.jpg"
            },
            {
                "_id": "passengers",
                "name": "Passengers",
                "poster": "passengers.jpg"
            }
        ]
    },
    {
        _id: 'music-original-song',
        name: 'Best Original song.',
        movies: [
            {
                "_id": "jim",
                "name": "'The Empty Chair' from Jim",
                "poster": "jackie.jpg"
            },
            {
                "_id": "lalaland",
                "name": "'City of Stars' from La La Land",
                "poster": "lalaland.jpg",
                "academyWinner": true
            },
            {
                "_id": "lalaland2",
                "name": "'Audition' from La La Land",
                "poster": "lalaland5.jpg"
            },
            {
                "_id": "moana",
                "name": "'How Far I'll Go' from Moana",
                "poster": "moana.jpg"
            },
            {
                "_id": "trolls",
                "name": "'Can't Stop the Feeling' from Trolls",
                "poster": "trolls.jpg"
            }
        ]
    },
    {
        _id: 'production-design',
        name: 'Best Production design.',
        movies: [
            {
                "_id": "arrival",
                "name": "Arrival",
                "poster": "arrival.jpg"
            },
            {
                "_id": "fantastic-beasts",
                "name": "Fantastic Beasts and Where to Find Them",
                "poster": "fantasticbeasts2.jpg"
            },
            {
                "_id": "hailcesar",
                "name": "Hail, Cesar!",
                "poster": "hailcesar.jpg"
            },
            {
                "_id": "lalaland",
                "name": "La La Land",
                "poster": "lalaland.jpg",
                "academyWinner": true
            },
            {
                "_id": "passengers",
                "name": "Passengers",
                "poster": "passengers.jpg"
            }
        ]
    },
    {
        _id: 'short-film-animated',
        name: 'Best Animated Short film.',
        movies: [
            {
                "_id": "blind-vaysha",
                "name": "Blind Vaysha",
                "poster": "blindvaysha.jpg"
            },
            {
                "_id": "borrowed-time",
                "name": "Borrowed Time",
                "poster": "borrowedtime.jpg"
            },
            {
                "_id": "pearcider",
                "name": "Pear Cider and Cigarettes",
                "poster": "pearcider.jpg"
            },
            {
                "_id": "pearl",
                "name": "Pearl",
                "poster": "pearl.jpg"
            },
            {
                "_id": "piper",
                "name": "Piper",
                "poster": "piper2.jpg",
                "academyWinner": true
            }
        ]
    },
    {
        _id: 'short-film-live-action',
        name: 'Best Live action Short film.',
        movies: [
            {
                "_id": "ennemis interieurs",
                "name": "Ennemis Interieurs",
                "poster": "ennemisinterieurs.jpg"
            },
            {
                "_id": "la-femme",
                "name": "La Femme et le TGV",
                "poster": "lafemme.jpg"
            },
            {
                "_id": "silent-nights",
                "name": "Silent Nights",
                "poster": "silentnights.jpg"
            },
            {
                "_id": "sing",
                "name": "Sing",
                "poster": "sing.jpg",
                "academyWinner": true
            },
            {
                "_id": "timecode",
                "name": "Timecode",
                "poster": "timecode.jpg"
            }
        ]
    },
    {
        _id: 'sound-editing',
        name: 'Best Sound editing.',
        movies: [
            {
                "_id": "arrival",
                "name": "Arrival",
                "poster": "arrival.jpg",
                "academyWinner": true
            },
            {
                "_id": "deepwater-horizon",
                "name": "Deepwater Horizon",
                "poster": "deepwaterhorizon.jpg"
            },
            {
                "_id": "hacksawridge",
                "name": "Hacksaw Ridge",
                "poster": "hacksaw.jpg"
            },
            {
                "_id": "lalaland",
                "name": "La La Land",
                "poster": "lalaland.jpg"
            },
            {
                "_id": "sully",
                "name": "Sully",
                "poster": "sully.jpg"
            }
        ]
    },
    {
        _id: 'sound-mixing',
        name: 'Best Sound mixing.',
        movies: [
            {
                "_id": "13-hours",
                "name": "13 Hours: The Sected Soldiers of Benghazi",
                "poster": "13hours2.jpg"
            },
            {
                "_id": "deepwater-horizon",
                "name": "Deepwater Horizon",
                "poster": "deepwaterhorizon.jpg"
            },
            {
                "_id": "hacksawridge",
                "name": "Hacksaw Ridge",
                "poster": "hacksaw.jpg",
                "academyWinner": true
            },
            {
                "_id": "lalaland",
                "name": "La La Land",
                "poster": "lalaland.jpg"
            },
            {
                "_id": "rogue-one",
                "name": "Rogue One: a Star Wars Story",
                "poster": "rogueone2.jpg"
            }
        ]
    },
    {
        _id: 'visual-effects',
        name: 'Best Visual effects.',
        movies: [
            {
                "_id": "deepwater-horizon",
                "name": "Deepwater Horizon",
                "poster": "deepwaterhorizon.jpg"
            },
            {
                "_id": "doctor-strange",
                "name": "Doctor Strange",
                "poster": "doctorstrange.jpg"
            },
            {
                "_id": "kubo",
                "name": "Kubo and the Two Strings",
                "poster": "kubo3.jpg"
            },
            {
                "_id": "rogue-one",
                "name": "Rogue One: a Star Wars Story",
                "poster": "rogueone2.jpg"
            },
            {
                "_id": "the-jungle-book",
                "name": "The Jungle Book",
                "poster": "thejunglebook3.jpg",
                "academyWinner": true
            }
        ]
    },
    {
        _id: 'writing-adapted-screenplay)',
        name: 'Best Adapted screenplay.',
        movies: [
            {
                "_id": "arrival",
                "name": "Arrival",
                "poster": "arrival.jpg"
            },
            {
               "_id": "fences",
                "name": "Fences",
                "poster": "fences.jpg"
            },
            {
                "_id": "hiddenfigures",
                "name": "Hidden Figures",
                "poster": "hiddenfigures3.jpg"
            },
            {
                "_id": "lion",
                "name": "Lion",
                "poster": "lion3.jpg"
            },
            {
                "_id": "moonlight",
                "name": "Moonlight",
                "poster": "moonlight.jpg",
                "academyWinner": true
            }
        ]
    },
    {
        _id: 'writing-original-screenplay)',
        name: 'Best Original screenplay.',
        movies: [
            {
                "_id": "20th-century-women",
                "name": "20th Century Women",
                "poster": "20thcenturywomen.jpg"
            },
            {
               "_id": "hellorhighwater",
                "name": "Hell or High Water",
                "poster": "hellorhighwater.jpg"
            },
            {
                "_id": "lalaland",
                "name": "La La Land",
                "poster": "lalaland.jpg"
            },
            {
                "_id": "lobster",
                "name": "Lobster",
                "poster": "lobster.jpg"
            },
            {
                "_id": "manchester-by-the-sea",
                "name": "Manchester by the Sea",
                "poster": "manchester4.jpg",
                "academyWinner": true
            }
        ]
    }
];