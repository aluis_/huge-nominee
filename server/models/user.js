var db = require('./db.js'),
    mongoose = require('mongoose'),
    userSchema;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function (callback) {
    console.log('db connected');
});

userSchema = mongoose.Schema({
    googleId: String,
    token: String,
    name: String,
    email: String,
    image: String,
    votes: mongoose.Schema.Types.Mixed
});

exports.User = mongoose.model('User', userSchema);